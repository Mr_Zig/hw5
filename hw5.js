// 1
console.log('Список студентов:');
var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
for (var i=0, imax=studentsAndPoints.length; i<imax; i+=2) {
    console.log('Студент '+studentsAndPoints[i]+' набрал '+studentsAndPoints[i+1]+' баллов');
  }

//2
console.log('Студент набравший максимальный балл:');
for (var i=0, maxBall=0, imax=studentsAndPoints.length; i<imax; i+=2) {
  if (studentsAndPoints[i+1] > maxBall ) {
    maxBall=studentsAndPoints[i+1];
    bestStudent=studentsAndPoints[i];
  }
}
console.log('Студент %s имеет максимальный балл %d', bestStudent, maxBall);

//3
studentsAndPoints.push ('Николай Фролов', 0, 'Олег Боровой', 0);

//4
studentsAndPoints[studentsAndPoints.indexOf('Антон Павлович')+1]+=10;
studentsAndPoints[studentsAndPoints.indexOf('Николай Фролов')+1]+=10;
console.log(studentsAndPoints);

// Для проверки удаления студентов не набравших баллы
// for (var i=0, imax=studentsAndPoints.length; i<imax; i+=2) {
//   studentsAndPoints[i+1]=0;
// }

// 5
console.log('Студенты не набравшие баллов:');
for (var i=0, imax=studentsAndPoints.length; i<imax; i+=2) {
  if (studentsAndPoints[i+1] === 0) {
    console.log(studentsAndPoints[i]);
    //6
    studentsAndPoints.splice([i],2);
    i-=2;

  }
}
console.log(studentsAndPoints);
